# OpenML dataset: Metro-Manila-Flood-Landscape-Data

https://www.openml.org/d/43459

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Latitude - lat
Longitude - lon
Flood Height - flood_height
0 - No flood
1 - Ankle High
2 - Knee High
3 - Waist High
4 - Neck High
5 - Top of Head High
6 - 1-storey High
7 - 1.5-storey High
8 - 2-storeys or Higher
Elevation - elevation (meters)
Precipitation - precipitat (millimetres/hour)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43459) of an [OpenML dataset](https://www.openml.org/d/43459). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43459/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43459/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43459/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

